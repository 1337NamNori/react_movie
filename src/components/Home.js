import React from "react";

// Config
import {POSTER_SIZE, BACKDROP_SIZE, IMAGE_BASE_URL} from "../config";

// Components
import HeroImage from "./HeroImage";
import SearchBar from "./SearchBar";
import Grid from "./Grid";
import Thumb from "./Thumb";
import Spinner from "./Spinner";
import Button from "./Button";

// Hooks
import {useHomeFetch} from "../hooks/useHomeFetch";

// Images
import NoImage from '../images/no_image.jpg';
import {useSelector} from "react-redux";
import {selectMovies} from "../features/movie/movieSlice";

const Home = () => {
    const movies = useSelector(selectMovies);
    const {error, loading, setSearchTerm, searchTerm, setIsLoadingMore} = useHomeFetch();

    if (error) return (<div>Something went wrong...</div>);

    return (
        <>
            {!searchTerm && movies.results[0] &&
                <HeroImage
                    image={`${IMAGE_BASE_URL}${BACKDROP_SIZE}${movies.results[0].backdrop_path}`}
                    title={movies.results[0].original_title}
                    text={movies.results[0].overview}
                />
            }
            <SearchBar setSearchTerm={setSearchTerm}/>
            <Grid header={searchTerm ? `Search Results for '${searchTerm}'` : 'Popular Movies'}>
                {movies.results.map(movie => (
                    <Thumb key={movie.id} clickable movieId={movie.id} movieTitle={movie.original_title}
                           image={
                               movie.poster_path
                                   ? IMAGE_BASE_URL + POSTER_SIZE + movie.poster_path
                                   : NoImage
                           }
                    />
                ))}
            </Grid>
            {loading && <Spinner />}
            {movies.page < movies.total_pages && !loading && (
                <Button text='Load more' callback={() => setIsLoadingMore(true)} />
            )}
        </>
    )
}

export default Home;