import {createSlice} from "@reduxjs/toolkit";

const initialState = {
    page: 0,
    results: [],
    total_pages: 0,
    total_results: 0,
}

const movieSlice = createSlice({
    name: 'movie',
    initialState,
    reducers: {
        setMovies: (state, action) => {
            state.page = action.payload.page;
            state.total_pages = action.payload.total_pages;
            state.total_results = action.payload.total_results;
            state.results = action.payload.results;
        }
    }
});

export const {setMovies} = movieSlice.actions;

export const selectMovies = (state) => state.movie;

export default movieSlice.reducer;